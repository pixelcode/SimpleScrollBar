# SimpleScrollBar

A simple, working scroll bar for HTML websites.

## **How it works**
As soon as the user scrolls down, the yellow scroll bar extends to the right. Scrolling up makes it collapse to the left.

**The scroll bar consists of 2 `divs`:**
```
<div id="progress-container" class="progress-container">
    <div class="progress-bar" id="myBar"></div>
</div>
```

**Scrolling calls the `scroll()` function**

```
window.onscroll = function() {scroll()};
```

**This is the actual `scroll()` function:**

```
function scroll() {
	var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
	var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
	var scrolled = (winScroll / height) * 100;
	document.getElementById("myBar").style.width = scrolled + "%";
	document.getElementById('progress-container').span = '';
	document.getElementById('progress-container').span.title = scrolled;
}
```

## **License**
You may freely use SimpleScrollBar according to the [MIT licence](https://codeberg.org/pixelcode/SimpleScrollBar/src/branch/master/LICENCE.md) (slightly modified). A link to this repo as the original source would be great 😎